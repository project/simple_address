<?php

namespace Drupal\simple_address\Plugin\Field\FieldType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\simple_address\Event\AvailableCountriesEvent;

/**
 * Allows field types to limit the available countries.
 */
trait AvailableCountriesTrait {

  /**
   * An altered list of available countries.
   *
   * @var array
   */
  protected static $availableCountries = [];

  /**
   * Defines the default field-level settings.
   *
   * @return array
   *   A list of default settings, keyed by the setting name.
   */
  public static function defaultCountrySettings() {
    return [
      'available_countries' => [],
      'administrative_division_field_label' => 'State/Province',
    ];
  }

  /**
   * Builds the field settings form.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the (entire) configuration form.
   *
   * @return array
   *   The modified form.
   */
  public function countrySettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];
    $element['available_countries'] = [
      '#type' => 'select',
      '#title' => $this->t('Available countries'),
      '#description' => $this->t('If no countries are selected, all countries will be available.'),
      '#options' => \Drupal::service('simple_address.address_repository')->getCountryOptions(),
      '#default_value' => $this->getSetting('available_countries'),
      '#multiple' => TRUE,
      '#size' => 10,
    ];
    $element['administrative_division_field_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Administrative division field label'),
      '#description' => $this->t('By default we use "State/Province" as an administrative division name.'),
      '#default_value' => $this->getSetting('administrative_division_field_label'),
      '#size' => 10,
    ];

    return $element;
  }

  /**
   * Gets the available countries for the current field.
   *
   * @return array
   *   A list of country codes.
   */
  public function getAvailableCountries() {
    // Alter the list once per field, instead of once per field delta.
    $field_definition = $this->getFieldDefinition();
    $definition_id = spl_object_hash($field_definition);
    if (!isset(static::$availableCountries[$definition_id])) {
      $available_countries = array_filter($this->getSetting('available_countries'));
      $event_dispatcher = \Drupal::service('event_dispatcher');
      $event = new AvailableCountriesEvent($available_countries, $field_definition);
      $event_dispatcher->dispatch($event, 'simple_address.available_countries');
      static::$availableCountries[$definition_id] = $event->getAvailableCountries();
    }

    return static::$availableCountries[$definition_id];
  }

}
