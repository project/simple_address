<?php

namespace Drupal\simple_address\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\simple_address\Plugin\Field\FieldType\Address;

/**
 * Implementation of the 'simple_address_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "simple_address_formatter",
 *   label = @Translation("Simple Address formatter"),
 *   field_types = {
 *     "simple_address"
 *   }
 * )
 */
class AddressFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    // This code is not tested. Probably it is not needed since we output
    // in the template.
    foreach ($items as $delta => $item) {
      $view = '';
      $full_address = [
        ['address_line'],
        ['state', 'city', 'postal_code'],
        ['country_code'],
      ];
      foreach ($full_address as $column) {
        foreach ($column as $field) {
          $value = $this->viewValue($item, $field);
          if (!$value) {
            continue;
          }
          if ($field == "state" || $field == 'city') {
            $view .= $this->viewValue($item, $field) . ", ";
          }
          else {
            $view .= $this->viewValue($item, $field) . " ";
          }
        }
        $view .= "<br>";
      }

      $elements[$delta] = ['#markup' => $view];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   * @param string $column
   *   The field column name to display.
   *
   * @return string|false
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item, string $column) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    $value = $item->{$column};
    if (!($item instanceof Address)) {
      return FALSE;
    }
    if ($column == 'country_code') {
      $value = $item->getCountryName();
    }
    if ($column == 'state') {
      $value = $item->getStateName();
    }
    return nl2br(Html::escape($value));
  }

}
