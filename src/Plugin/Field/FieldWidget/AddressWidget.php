<?php

namespace Drupal\simple_address\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\simple_address\AddressRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'simple_address_widget' widget.
 *
 * @FieldWidget(
 *   id = "simple_address_widget",
 *   module = "simple_address",
 *   label = @Translation("Simple Address widget"),
 *   field_types = {
 *     "simple_address"
 *   }
 * )
 */
class AddressWidget extends WidgetBase {

  use DependencySerializationTrait;

  /**
   * The address repository service.
   */
  protected AddressRepository $addressRepository;

  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    AddressRepository $address_repository
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->addressRepository = $address_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('simple_address.address_repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $empty_option = ['' => $this->t('Choose option')];
    $country_options = $empty_option;
    $available_countries = $items[$delta]->getAvailableCountries();
    $country_options += $this->getCountryOptions($available_countries);

    $state_field_id_array = ['state'];
    $state_field_id_array += $element['#field_parents'];
    $state_field_id_array[] = $items->getName();
    $state_field_id_array[] = $delta;
    $state_field_id = Html::getId(implode('-', $state_field_id_array));

    $element['#type'] = 'container';
    $element['#address_widget'] = TRUE;
    $element['country_code'] = [
      '#type' => 'select',
      '#default_value' => $items[$delta]->country_code ?? NULL,
      '#required' => $element['#required'],
      '#title' => $this->t('Country'),
      '#title_display' => $element['#title_display'],
      '#options' => $country_options,
      '#ajax' => [
        'callback' => [$this, 'stateAjaxCallback'],
        'wrapper' => $state_field_id,
        'method' => 'replace',
      ],
    ];

    $country_code = $element['country_code']['#default_value'];

    $state_options = $empty_option;
    $state_options += $this->getStateOptions($country_code);
    $administrative_division_field_label = $items[$delta]->getFieldDefinition()->getSettings()['administrative_division_field_label'];
    $element['state'] = [
      '#type' => 'select',
      '#title' => $administrative_division_field_label,
      '#default_value' => $items[$delta]->state ?? NULL,
      '#options' => $state_options,
      '#title_display' => $element['#title_display'],
      '#validated' => TRUE,
      '#wrapper_attributes' => [
        'id' => $state_field_id,
      ],
    ];

    $element['city'] = [
      '#title' => $this->t('City'),
      '#type' => 'textfield',
      '#title_display' => $element['#title_display'],
      '#default_value' => $items[$delta]->city ?? NULL,
      '#placeholder' => $this->t('Enter city'),
    ];

    $element['address_line'] = [
      '#title' => $this->t('Address'),
      '#type' => 'textfield',
      '#title_display' => $element['#title_display'],
      '#placeholder' => $this->t('Enter street name, house number, apt'),
      '#default_value' => $items[$delta]->address_line ?? NULL,
    ];

    $element['postal_code'] = [
      '#title' => $this->t('Postal code'),
      '#type' => 'textfield',
      '#title_display' => $element['#title_display'],
      '#placeholder' => $this->t('Enter postal code'),
      '#default_value' => $items[$delta]->postal_code ?? NULL,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function stateAjaxCallback(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();

    $empty_option = ['' => $this->t('Choose option')];
    $state_options = $empty_option;
    $triggering_element = $form_state->getTriggeringElement();

    $parents = $triggering_element['#array_parents'];
    array_pop($parents);
    $parents[] = 'state';
    $state_element = NestedArray::getValue($form, $parents);

    $added_state_options = $this->getStateOptions($triggering_element['#value']);
    if (reset($added_state_options) === 'No states associated with this country.' || empty($added_state_options)) {
      $state_options = $this->getStateOptions($triggering_element['#value']);
      if (empty($state_options)) {
        $state_options = $empty_option;
      }
    }
    else {
      $state_element["#required"] = TRUE;
      $state_options += $this->getStateOptions($triggering_element['#value']);
    }

    $state_element['#options'] = $state_options;

    return $state_element;
  }

  /**
   * Construct the countries options.
   *
   * @param array $available_countries
   *   Available countries.
   */
  private function getCountryOptions(array $available_countries = []): array {
    // phpcs:disable
    $options = $this->addressRepository->getCountryOptions();
    // For some reason, DI doesn't work here with ajax.
    // phpcs:enable

    if (!empty($available_countries)) {
      $options = array_filter($options, function ($value) use ($available_countries) {
        return in_array($value, $available_countries);
      }, ARRAY_FILTER_USE_KEY);
    }

    return $options;
  }

  /**
   * Build options for a country element.
   *
   * @param string|null $country_code
   *   The country alpha 3 code.
   *
   * @return array
   *   The option array.
   */
  private function getStateOptions(string $country_code = NULL): array {
    $options = [];
    if (!$country_code) {
      return $options;
    }
    // phpcs:disable
    // For some reason, DI doesn't work here with ajax.
    // phpcs:enable
    $states = $this->addressRepository->findSubDivision($country_code);
    foreach ($states as $state) {
      $options[$state['state_code']] = $state['state'];
    }

    return $options;
  }

}
