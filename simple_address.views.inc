<?php

/**
 * @file
 * Provide Views data for the Simple Address module.
 *
 * @ingroup views_module_handlers
 */

use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_field_views_data().
 *
 * Views integration for simple address fields.
 */
function simple_address_field_views_data(FieldStorageConfigInterface $field) {
  $data = [];
  $field_type = $field->getType();
  if ($field_type == 'simple_address') {
    $data = views_field_default_views_data($field);
    $field_name = $field->getName();

    $columns = [
      'country_code' => 'simple_address_country',
      'state' => 'simple_address_state',
      'postal_code' => 'standard',
      'city' => 'standard',
      'address_line' => 'standard',
    ];
    foreach ($data as $table_name => $table_data) {
      foreach ($columns as $column => $plugin_id) {
        $data[$table_name][$field_name . '_' . $column]['field'] = [
          'id' => $plugin_id,
          'field_name' => $field_name,
          'property' => $column,
        ];
      }
      $data[$table_name][$field_name . '_country_code']['filter']['id'] = 'simple_address_country';
    }
  }

  return $data;
}
