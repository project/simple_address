<?php

namespace Drupal\simple_address\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'simple_address' field type.
 *
 * @FieldType(
 *   id = "simple_address",
 *   label = @Translation("Simple Address"),
 *   description = @Translation("Provide address field with flat subdivisions"),
 *   default_widget = "simple_address_widget",
 *   default_formatter = "simple_address_formatter"
 * )
 */
class Address extends FieldItemBase {

  use AvailableCountriesTrait;

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'country_code' => [
          'type' => 'varchar',
          'length' => 3,
        ],
        'state' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'postal_code' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'city' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'address_line' => [
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Prevent early t() calls by using the TranslatableMarkup.
    $properties['country_code'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Country'))
      ->setRequired(TRUE);

    $properties['state'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('State/province'))
      ->setRequired(FALSE);

    $properties['postal_code'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Postal Code'))
      ->setRequired(FALSE);

    $properties['city'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('City'))
      ->setRequired(FALSE);

    $properties['address_line'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Your full address'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('country_code')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return self::defaultCountrySettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    return $this->countrySettingsForm($form, $form_state);
  }

  /**
   * Get country code.
   */
  public function getCountryCode() {
    return $this->get('country_code')->getValue();
  }

  /**
   * Get country name. Need to load the repository.
   */
  public function getCountryName() {
    $address_repository = \Drupal::service('simple_address.address_repository');
    $countries = $address_repository->getCountries();
    return $countries[$this->get('country_code')->getValue()]['name'];
  }

  /**
   * Get the postal code.
   */
  public function getPostalCode() {
    return $this->get('postal_code')->getValue();
  }

  /**
   * Get the city line.
   */
  public function getCity() {
    return $this->get('city')->getValue();
  }

  /**
   * Get the address line.
   */
  public function getAddressLine() {
    return $this->get('address_line')->getValue();
  }

  /**
   * Get the state code.
   */
  public function getStateCode() {
    return $this->get('state')->getValue();
  }

  /**
   * Get the state human name.
   */
  public function getStateName() {
    $address_repository = \Drupal::service('simple_address.address_repository');
    $country = $this->getCountryCode();
    $states = $address_repository->findSubDivision($country);
    $state_codes = array_column($states, 'state_code');
    $current_state_key = array_search($this->getStateCode(), $state_codes);
    return $states[$current_state_key]['state'];
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = new Random();
    $values['city'] = $random
      ->word(mt_rand(1, 20));
    $values['address_line'] = $random
      ->word(mt_rand(1, 128));
    $values['postal_code'] = $random
      ->word(mt_rand(1, 10));

    $address_repository = \Drupal::service('simple_address.address_repository');
    $countries = $address_repository->getCountries();
    $country = array_rand($countries);
    $values['country_code'] = $country;

    $states = $address_repository->findSubDivision($country);
    $state = array_rand($states);
    $values['state'] = $states[$state]['state_code'];

    return $values;
  }

}
