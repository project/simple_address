<?php

namespace Drupal\simple_address\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\simple_address\AddressRepository;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Allows the country name to be displayed instead of the country code.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("simple_address_country")
 */
class Country extends FieldPluginBase {

  /**
   * The address repository.
   *
   * @var \Drupal\simple_address\AddressRepository
   */
  protected $addressRepository;

  /**
   * Constructs a Country object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The id of the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\simple_address\AddressRepository $address_repository
   *   The address repository.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AddressRepository $address_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->addressRepository = $address_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('simple_address.address_repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['display_name'] = ['default' => TRUE];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['display_name'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display the country name instead of the country code'),
      '#default_value' => !empty($this->options['display_name']),
    ];
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $value = $this->getValue($values);

    if (!empty($this->options['display_name']) && !empty($value)) {
      $countries = $this->addressRepository->getCountries();
      if (isset($countries[$value])) {
        $value = $countries[$value]['name'];
      }
    }

    return $this->sanitizeValue($value);
  }

}
