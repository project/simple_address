<?php

namespace Drupal\simple_address\Plugin\views\filter;

/**
 * Filter by country.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("simple_address_country")
 */
class Country extends CountryAwareInOperatorBase {

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    if (!isset($this->valueOptions)) {
      $this->valueOptions = $this->getAvailableCountries();
    }

    return $this->valueOptions;
  }

}
