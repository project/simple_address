<?php

namespace Drupal\simple_address\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\simple_address\Plugin\Field\FieldType\Address;

/**
 * Implementation of the 'simple_address_formatter_configurable' formatter.
 *
 * @FieldFormatter(
 *   id = "simple_address_formatter_configurable",
 *   label = @Translation("Simple Address formatter configurable"),
 *   field_types = {
 *     "simple_address"
 *   }
 * )
 */
class AddressFormatterConfigurable extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'country_code' => TRUE,
      'country_code_name' => FALSE,
      'state' => TRUE,
      'state_name' => FALSE,
      'address_line' => TRUE,
      'postal_code' => TRUE,
      'city' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $fieldName = $this->fieldDefinition->getName();
    $formFieldsDefinitions = $this->formFieldsDefinitions($fieldName);

    foreach ($formFieldsDefinitions as $field => $definition) {
      $form[$field] = $definition;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    // This code is not tested. Probably it is not needed since we output
    // in the template.
    foreach ($items as $delta => $item) {
      $values = [];
      $fields = [
        'country_code',
        'state',
        'address_line',
        'postal_code',
        'city',
      ];
      foreach ($fields as $field) {
        if (!$this->getSetting($field)) {
          continue;
        }
        $value = $this->viewValue($item, $field);
        if (!$value) {
          continue;
        }
        $values[$field] = $value;
      }

      $elements[$delta] = [
        '#theme' => 'simple_address_formatter',
        '#values' => $values,
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    return $this->getSettingsSummary();
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   * @param string $column
   *   The field column name to display.
   *
   * @return string|false
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item, string $column) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    if (!($item instanceof Address)) {
      return FALSE;
    }
    $value = $item->{$column};
    if ($column === 'country_code') {
      if ($this->getSetting('country_code_name')) {
        $value = $item->getCountryName();
      }
    }
    if ($column === 'state') {
      if ($this->getSetting('state_name')) {
        $value = $item->getStateName();
      }
    }
    return nl2br(Html::escape($value));
  }

  /**
   * Gets settings summary.
   */
  private function getSettingsSummary(): array {
    $summary = [];

    $fields = [
      'country_code' => 'Show country code',
      'country_code_name' => 'Show country name instead of country code',
      'state' => 'Show state',
      'state_name' => 'Show state name instead of state code',
      'address_line' => 'Show address line',
      'postal_code' => 'Show postal code',
      'city' => 'Show city',
    ];

    if (!$this->getSetting('country_code')) {
      unset($fields['country_code_name']);
    }

    if (!$this->getSetting('state')) {
      unset($fields['state_name']);
    }

    foreach ($fields as $field => $value) {
      $summary[] = $this->t("@field_summary: @status", [
        '@status' => $this->getSetting($field) ? 'Enabled' : 'Disabled',
        '@field_summary' => $value,
      ]);
    }

    return $summary;
  }

  /**
   * Returns form fields definitions.
   */
  public function formFieldsDefinitions(string $fieldName): array {
    return [
      'country_code' => [
        '#title' => $this->t('Show country code'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('country_code'),
      ],
      'country_code_name' => [
        '#title' => $this->t('Show country name instead of country code'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('country_code_name'),
        '#states' => [
          'visible' => [
            ':input[name="fields[' . $fieldName . '][settings_edit_form][settings][country_code]"]' => ['checked' => TRUE],
          ],
        ],
      ],
      'state' => [
        '#title' => $this->t('Show state'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('state'),
      ],
      'state_name' => [
        '#title' => $this->t('Show state name instead of state code'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('state_name'),
        '#states' => [
          'visible' => [
            ':input[name="fields[' . $fieldName . '][settings_edit_form][settings][state]"]' => ['checked' => TRUE],
          ],
        ],
      ],
      'address_line' => [
        '#title' => $this->t('Show address line'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('address_line'),
      ],
      'postal_code' => [
        '#title' => $this->t('Show postal code'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('postal_code'),
      ],
      'city' => [
        '#title' => $this->t('Show city'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('city'),
      ],
    ];
  }

}
