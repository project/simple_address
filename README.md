# Simple Address Module

## Introduction

The well-known [Address](https://www.drupal.org/project/address) module provides extensive address options with validations and different address fields for various countries. This is an excellent solution for users requiring addresses for delivery, billing, etc. However, many content sites need a straightforward address solution without strict validations, yet with user assistance when selecting state/province for the chosen country. This module aims to address that need.

It utilizes the country/state & province repository from the Address module.

## Usage

1. Install the module.
2. Add the Simple Address field to the desired entity.
3. Use it as a regular field in Drupal.

## TODO

1. Move the list of available countries to a field widget settings (or field formatter).
2. Allow altering the list of plausible countries and states.

## Maintainers

- [Dstorozhuk](https://www.drupal.org/u/dstorozhuk)
- [Goodboy](https://www.drupal.org/u/goodboy)
